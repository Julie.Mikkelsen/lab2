package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    
    int max_size = 20;
    List <FridgeItem> foodList = new ArrayList<>();
    
    @Override
    public int totalSize(){
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        return foodList.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (foodList.size() < max_size) {
            foodList.add(item);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (foodList.contains(item)) {
            foodList.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        if (foodList.size() > 0 ) {
            foodList.clear();
        }
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for(int i=0; i < nItemsInFridge(); i++) {
            FridgeItem item = foodList.get(i);
            if(item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        for (FridgeItem expiredItem : expiredFood) {
            foodList.remove(expiredItem);
        }
        return expiredFood;
    }
    
}
